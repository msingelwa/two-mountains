import { environment } from "./../../../environments/environment.prod";
import { MakeArrangementComponent } from "./../arrangements/make-arrangement/make-arrangement.component";
import { ArrangementsComponent } from "./../arrangements/arrangements.component";
import { Component, OnInit, QueryList, ViewChildren } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable } from "rxjs";
import {
  AdvancedSortableDirective,
  SortEvent,
} from "../claims/advanced-sortable.directive";
import { Table } from "../claims/advanced.model";
import { AdvancedService } from "../claims/advanced.service";
import { tableData } from "../claims/data";
import * as firebase from "firebase";

@Component({
  selector: "app-collections",
  templateUrl: "./collections.component.html",
  styleUrls: ["./collections.component.scss"],
  providers: [AdvancedService, NgbModal],
})
export class CollectionsComponent implements OnInit {
  breadCrumbItems: Array<{}>;
  hideme: boolean[] = [];
  tableData: Table[] = [];

  tables$: Observable<Table[]>;
  total$: Observable<number>;

  @ViewChildren(AdvancedSortableDirective)
  headers: QueryList<AdvancedSortableDirective>;
  collection: Table;

  constructor(public service: AdvancedService, private modalService: NgbModal) {
    this.tables$ = service.tables$;
    this.total$ = service.total$;
  }

  ngOnInit(): void {
    // let f = firebase.initializeApp(environment.firebase);

    this.breadCrumbItems = [
      { label: "Two Mountains" },
      { label: "Collections", active: true },
    ];
    // firebase.initializeApp(environment.firebase);
    this._fetchData();
  }

  arrange(data: Table) {
    this.collection = data;
    const modalRef = this.modalService.open(MakeArrangementComponent, {
      size: "lg",
      centered: true,
    });
    modalRef.componentInstance.collection = this.collection;
  }

  changeValue(i) {
    this.hideme[i] = !this.hideme[i];
  }

  /**
   * fetches the table value
   */
  _fetchData() {
    const dbRef = firebase.database().ref();
    const collectionsRef = dbRef.child("collections");

    collectionsRef.on("child_added", (snap) => {
      let data = snap.val();
      data.key = snap.key;

      console.log(data.id);

      this.tableData.push(data as Table);
    });
  }

  /**
   * Sort table data
   * @param param0 sort the column
   *
   */
  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach((header) => {
      if (header.sortable !== column) {
        header.direction = "";
      }
    });
    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }
}
