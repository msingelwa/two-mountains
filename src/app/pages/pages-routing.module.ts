import { MakeClaimComponent } from "./claims/make-claim/make-claim.component";
import { ViewComponent } from "./arrangements/view/view.component";
import { MakeArrangementComponent } from "./arrangements/make-arrangement/make-arrangement.component";
import { ArrangementsComponent } from "./arrangements/arrangements.component";
import { CollectionsComponent } from "./collections/collections.component";
import { ClaimsComponent } from "./claims/claims.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CalendarComponent } from "./calendar/calendar.component";
import { DashboardComponent } from "./dashboard/dashboard.component";

const routes: Routes = [
  { path: "", component: DashboardComponent },
  { path: "calendar", component: CalendarComponent },
  { path: "claims", component: ClaimsComponent },
  { path: "collections", component: CollectionsComponent },
  {
    path: "arrangements",
    component: ArrangementsComponent,
    children: [
      {
        path: ":id",
        component: ViewComponent,
      },
    ],
  },
  { path: "make-arrangement", component: MakeArrangementComponent },
  { path: "make-claim/:id", component: MakeClaimComponent },
  { path: ":id", component: ViewComponent },
  {
    path: "charts",
    loadChildren: () =>
      import("./chart/chart.module").then((m) => m.ChartModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
