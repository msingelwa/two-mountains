import { FormGroup, FormBuilder } from "@angular/forms";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { WizardComponent } from "angular-archwizard";
import * as firebase from "firebase";

@Component({
  selector: "app-view",
  templateUrl: "./view.component.html",
  styleUrls: ["./view.component.scss"],
  providers: [WizardComponent],
})
export class ViewComponent implements OnInit {
  breadCrumbItems: Array<{}>;
  @Input() arrangement: any;
  viewForm: FormGroup;

  constructor(
    public modal: NgbActiveModal,
    private router: Router,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.breadCrumbItems = [
      { label: "Two Mountains" },
      { label: "View Arrangement", active: true },
    ];
    this.init();

    this.viewForm.patchValue({
      fullnames: this.arrangement.fullName,
      address: this.arrangement.address,
      policy: this.arrangement.policy,
      timeofservice: this.arrangement.timeOfService,
      typeofservice: this.arrangement.typeOfService,
      pickupplace: this.arrangement.placeOfPickUp,
      dod: this.arrangement.dateOfDeath,
      graveNo: this.arrangement.graveNumber,
    });
  }

  makeClaim() {
    this.router.navigate(["make-claim", this.arrangement.id]);
    this.modal.close();
  }

  init() {
    this.viewForm = this.fb.group({
      fullnames: [],
      address: [],
      policy: [],
      timeofservice: [],
      typeofservice: [],
      pickupplace: [],
      dod: [],
      graveNo: [],
    });
  }

  fetchData() {
    firebase
      .firestore()
      .collection("arrangement")
      .onSnapshot((snapShot) => {
        let data;
        snapShot.docs.forEach((doc) => {
          if (doc.exists) {
            data = doc.data();
            const obj = JSON.parse(data.obj);
            obj.id = doc.id;
          }
        });
      });
  }
}
