import { Table } from "./../../claims/advanced.model";
import { FormGroup, FormBuilder } from "@angular/forms";
import { Component, OnInit, Input } from "@angular/core";
import { NgbActiveModal, NgbToast } from "@ng-bootstrap/ng-bootstrap";
import { firestore } from "firebase";
import * as firebase from "firebase";

@Component({
  selector: "app-make-arrangement",
  templateUrl: "./make-arrangement.component.html",
  styleUrls: ["./make-arrangement.component.scss"],
})
export class MakeArrangementComponent implements OnInit {
  @Input() collection: Table;
  arrangementForm: FormGroup;
  tombStone: boolean;
  paidInFull: boolean;
  isThereViewing: boolean;
  constructor(private fb: FormBuilder, public modal: NgbActiveModal) {}

  onPaymentStatus(value) {
    if (value == "yes") {
      this.paidInFull = true;
    }
  }
  init() {
    this.arrangementForm = this.fb.group({
      surname: [null],
      fullName: [null],
      dateOfDeath: [null],
      placeOfDeath: [null],
      placeOfPickUp: [null],
      dateOfPickUp: [null],
      scheme: [""],
      bsurname: [null],
      bfullName: [],
      address: [],
      bContactNumber: [],
      bAlternativeNumber: [],
      postmoterm: [null],
      typeOfService: [null],
      placeOfDelivery: [null],
      placeOfService: [null],
      directionToLocation: [null],
      packageSelected: [null],
      coffinSelected: [null],
      additionalItems: [null],
      coffinSpray: [null],
      graveMarker: [null],
      policy: [null],
      isThereViewing: [null],
      graveNumber: [null],
      timeOfService: [null],
      timeOfDepartFromMortuary: [null],
      dateOfServiceAvailable: [null],
      tombStone: [null],
      terms: [null],
    });
  }
  ngOnInit(): void {
    this.init();

    this.arrangementForm.patchValue({
      fullName: this.collection.name,
      surname: this.collection.surname,
      dateOfDeath: this.collection.date || "03/02/2022",
      placeOfDeath: this.collection.collectedFrom,
      placeOfPickUp: this.collection.collectedFrom,
      dateOfPickUp: this.collection.date || "03/02/2022",
    });
  }

  save() {
    let obj = JSON.stringify(this.arrangementForm.value);
    // firebase
    //   .firestore()
    //   .collection("arrangement")
    //   .add({ obj })
    //   .then(() => {
    //     this.modal.close();
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });

    const dbRef = firebase.database().ref();
    const collectionsRef = dbRef.child("arrangements");

    collectionsRef
      .push({ obj })
      .then(() => {
        this.modal.close();
      })
      .catch((err) => console.log(err));
  }
}
