import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MakeArrangementComponent } from './make-arrangement.component';

describe('MakeArrangementComponent', () => {
  let component: MakeArrangementComponent;
  let fixture: ComponentFixture<MakeArrangementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MakeArrangementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MakeArrangementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
