import { ViewComponent } from "./view/view.component";
import { NgbActiveModal, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MakeArrangementComponent } from "./make-arrangement/make-arrangement.component";
import { Router } from "@angular/router";
import { Component, OnInit, QueryList, ViewChildren } from "@angular/core";
import { Observable } from "rxjs";
import {
  AdvancedSortableDirective,
  SortEvent,
} from "../claims/advanced-sortable.directive";
import { Arrangement, Table } from "../claims/advanced.model";
import { AdvancedService } from "../claims/advanced.service";
import { tableData } from "../claims/data";
import * as firebase from "firebase";

@Component({
  selector: "app-arrangements",
  templateUrl: "./arrangements.component.html",
  styleUrls: ["./arrangements.component.scss"],
  providers: [NgbActiveModal],
})
export class ArrangementsComponent implements OnInit {
  breadCrumbItems: Array<{}>;
  hideme: boolean[] = [];
  tableData: Arrangement[] = [];

  tables$: Observable<Table[]>;
  total$: Observable<number>;

  @ViewChildren(AdvancedSortableDirective)
  headers: QueryList<AdvancedSortableDirective>;

  constructor(
    public service: AdvancedService,
    private router: Router,
    private modalService: NgbModal
  ) {
    this.tables$ = service.tables$;
    this.total$ = service.total$;
  }

  ngOnInit(): void {
    this.breadCrumbItems = [
      { label: "Two Mountains" },
      { label: "Arrangements", active: true },
    ];
    this._fetchData();
  }

  changeValue(i) {
    this.hideme[i] = !this.hideme[i];
  }

  view(data: any) {
    // this.router.navigate(["arrangements/", id]);
    const modalRef = this.modalService.open(ViewComponent, {
      size: "lg",
      centered: true,
    });

    modalRef.componentInstance.arrangement = data;
  }

  /**
   * fetches the table value
   */
  _fetchData() {
    // firebase
    //   .firestore()
    //   .collection("arrangement")
    //   .onSnapshot((snapShot) => {
    //     let data;
    //     snapShot.docs.forEach((doc) => {
    //       if (doc.exists) {
    //         data = doc.data();
    //         const obj = JSON.parse(data.obj);
    //         obj.id = doc.id;

    //         this.tableData.push(obj as Arrangement);
    //       }
    //     });
    //   });

    const dbRef = firebase.database().ref();
    const collectionsRef = dbRef.child("arrangements");

    collectionsRef.on("child_added", (snap) => {
      if (snap.exists) {
        let data = snap.val();

        const obj = JSON.parse(data.obj);
        obj.id = snap.key;
        console.log(obj);

        this.tableData.push(obj as Arrangement);
      }
    });
  }

  makeClaim(data: any) {
    this.router.navigate(["make-claim", data.id]);
  }

  /**
   * Sort table data
   * @param param0 sort the column
   *
   */
  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach((header) => {
      if (header.sortable !== column) {
        header.direction = "";
      }
    });
    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }
}
