// Table data
export interface Table {
  name: string;
  surname: string;
  informant: string;
  informantNumber: number;
  timeNow: string;
  idNO: string;
  collectedFrom: string;
  date: string;
}
export interface Arrangement {
  fullName: string;
  pickUp: string;
  dateOfDeath: string;
  policy: number;
  funeralDate: string;
  delivery: string;
  time: string;
  typeOfService: string;
}
export interface Claim {
  //{"fullnames":null,"dateOfDeath":"03/02/2022",
  //"entryDate": null, "address": "127 Cape Rd",
  //"policy": "452", "claimNo": "CLMESPS8", "id": null, "placeOfPickUp": "Hhfhffj",
  //"placeOfDeath": "Hhfhffj", "contact": null, "plan": null, "payment": null, "bank": null
  policy: string;
  claimNo: string;
  pickPlace: string;
  idPassport: string;
  contact: string;
  plan: string;
  dateOfDeath: string;
  address: string;
}

// Search Data
export interface SearchResult {
  tables: Table[];
  total: number;
}
