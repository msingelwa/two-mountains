import { Component, OnInit, QueryList, ViewChildren } from "@angular/core";
import * as firebase from "firebase";
import { Observable } from "rxjs";
import {
  AdvancedSortableDirective,
  SortEvent,
} from "./advanced-sortable.directive";
import { Claim, Table } from "./advanced.model";
import { AdvancedService } from "./advanced.service";
import { tableData } from "./data";

@Component({
  selector: "app-claims",
  templateUrl: "./claims.component.html",
  styleUrls: ["./claims.component.scss"],
  providers: [AdvancedService],
})
export class ClaimsComponent implements OnInit {
  breadCrumbItems: Array<{}>;
  hideme: boolean[] = [];
  tableData: any[] = [];

  tables$: Observable<any[]>;
  total$: Observable<number>;

  @ViewChildren(AdvancedSortableDirective)
  headers: QueryList<AdvancedSortableDirective>;

  constructor(public service: AdvancedService) {
    this.tables$ = service.tables$;
    this.total$ = service.total$;
    this.fetchData();
  }

  ngOnInit(): void {
    this.breadCrumbItems = [
      { label: "Two Mountains" },
      { label: "Claims", active: true },
    ];
  }

  changeValue(i) {
    this.hideme[i] = !this.hideme[i];
  }

  /**
   * fetches the table value
   */
  fetchData() {
    // firebase
    //   .firestore()
    //   .collection("claims")
    //   .onSnapshot((snapShot) => {
    //     let data;
    //     snapShot.docs.forEach((doc) => {
    //       if (doc.exists) {
    //         data = doc.data();
    //         const obj = JSON.parse(data.obj);
    //         obj.id = doc.id;

    //         this.tableData.push(obj as Claim);
    //       }
    //     });
    //   });

    const dbRef = firebase.database().ref();
    const collectionsRef = dbRef.child("claims");

    collectionsRef.on("child_added", (snap) => {
      let data = snap.val();

      const obj = JSON.parse(data.obj);
      obj.id = snap.key;

      this.tableData.push(obj as Claim);
      console.log(tableData);
    });
  }

  /**
   * Sort table data
   * @param param0 sort the column
   *
   */
  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach((header) => {
      if (header.sortable !== column) {
        header.direction = "";
      }
    });
    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }
}
