import { NgbToast } from "@ng-bootstrap/ng-bootstrap";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import * as firebase from "firebase";

@Component({
  selector: "app-make-claim",
  templateUrl: "./make-claim.component.html",
  styleUrls: ["./make-claim.component.scss"],
})
export class MakeClaimComponent implements OnInit {
  makeClaimForm: FormGroup;
  breadCrumbItems: Array<{}>;
  id: string;
  constructor(
    private fb: FormBuilder,
    private routeActivated: ActivatedRoute,
    private router: Router
  ) {
    this.breadCrumbItems = [
      { label: "Two Mountains" },
      { label: "Make Claim", active: true },
    ];
    this.init();
    this.id = this.routeActivated.snapshot.paramMap.get("id");

    this.fetchData();
  }

  ngOnInit(): void {}

  init() {
    this.makeClaimForm = this.fb.group({
      fullnames: [],
      dateOfDeath: [],
      entryDate: [],
      packageSelected: [],
      address: [],
      policy: [],
      claimNo: [],
      idPassport: [],
      placeOfPickUp: [],
      placeOfDeath: [],
      bContactNumber: [],
      plan: [],
      payment: [],
      bank: [],
    });
  }

  fetchData() {
    // firebase
    //   .firestore()
    //   .collection("arrangement")
    //   .doc(this.id)
    //   .onSnapshot((snapShot) => {
    //     let data = JSON.parse(snapShot.data().obj);
    //     const result =
    //       "CLM" + Math.random().toString(36).substring(2, 7).toUpperCase();

    //     this.makeClaimForm.patchValue(data);
    //     this.makeClaimForm.patchValue({ claimNo: result });
    //   });

    const dbRef = firebase.database().ref();
    const collectionsRef = dbRef.child("arrangements");

    collectionsRef.on("child_added", (snap) => {
      if (snap.key == this.id) {
        let data = JSON.parse(snap.val().obj);
        data.id = snap.key;

        console.log(data);
        const result =
          "CLM" + Math.random().toString(36).substring(2, 7).toUpperCase();

        this.makeClaimForm.patchValue(data);
        this.makeClaimForm.patchValue({ claimNo: result });
        this.makeClaimForm.patchValue({
          packageSelected: data.packageSelected,
        });
        this.makeClaimForm.patchValue({
          bContactNumber: data.bContactNumber,
        });
      }
    });
  }
  save() {
    let obj = JSON.stringify(this.makeClaimForm.value);
    const dbRef = firebase.database().ref();
    const collectionsRef = dbRef.child("claims");
    // firebase
    //   .firestore()
    //   .collection("claims")
    //   .add({ obj })
    //   .then(() => {
    //     this.router.navigate(["claims"]);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });

    collectionsRef
      .push({ obj })
      .then(() => {
        this.router.navigate(["claims"]);
      })
      .catch((err) => console.log(err));
  }
}
