import { MenuItem } from "./menu.model";

export const MENU: MenuItem[] = [
  {
    id: 1,
    label: "Menu",
    isTitle: true,
  },
  {
    id: 2,
    label: "Dashboard",
    icon: " ri-dashboard-line",
    link: "/",
  },

  {
    id: 4,
    label: "Claims",
    icon: "ri-currency-line",
    link: "/claims",
  },
  {
    id: 5,
    label: "Arrangements",
    icon: "ri-calendar-line",
    link: "/arrangements",
  },
  {
    id: 3,
    label: "Collections",
    icon: "ri-calendar-check-line",
    link: "/collections",
  },
  {
    id: 6,
    label: "Employees",
    icon: "ri-group-line",
    link: "/employees",
  },
  {
    id: 7,
    label: "Settings",
    icon: "ri-settings-3-line",
    link: "/settings",
  },
  {
    id: 8,
    label: "Burial Services",
    icon: "ri-service-line",
    link: "/burial",
  },
  {
    id: 9,
    label: "Policies",
    icon: "ri-funds-line",
    link: "/policy",
  },
  {
    id: 10,
    label: "Inventory",
    icon: "ri-briefcase-line",
    link: "/inventory",
  },
  {
    id: 11,
    label: "Mortuaries",
    icon: " ri-building-2-line",
    link: "/mortuaries",
  },
];
