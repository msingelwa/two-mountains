export const environment = {
  firebase: {
    projectId: 'two-mountains-24d72',
    appId: '1:740481691299:web:ba4936a1b806f235e3d916',
    databaseURL: 'https://two-mountains-24d72-default-rtdb.firebaseio.com',
    storageBucket: 'two-mountains-24d72.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyDkSFs3gjXwdX4s-5gxGoUMlujtz-jFK2A',
    authDomain: 'two-mountains-24d72.firebaseapp.com',
    messagingSenderId: '740481691299',
  },
  production: true,
  defaultauth: 'fackbackend',
  firebaseConfig: {
    apiKey: '',
    authDomain: '',
    databaseURL: '',
    projectId: '',
    storageBucket: '',
    messagingSenderId: '',
    appId: '',
    measurementId: ''
  }
};
