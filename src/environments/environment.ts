// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: "two-mountains-24d72",
    appId: "1:740481691299:web:ba4936a1b806f235e3d916",
    databaseURL: "https://two-mountains-24d72-default-rtdb.firebaseio.com",
    storageBucket: "two-mountains-24d72.appspot.com",
    locationId: "us-central",
    apiKey: "AIzaSyDkSFs3gjXwdX4s-5gxGoUMlujtz-jFK2A",
    authDomain: "two-mountains-24d72.firebaseapp.com",
    messagingSenderId: "740481691299",
  },
  production: false,
  defaultauth: "fackbackend",
  firebaseConfig: {
    apiKey: "",
    authDomain: "",
    databaseURL: "",
    projectId: "",
    storageBucket: "",
    messagingSenderId: "",
    appId: "",
    measurementId: "",
  },
};

// Import the functions you need from the SDKs you need

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
